import java.util.WeakHashMap;

public class Main {
    public static void main(String[] args) {
        WeakHashMap<String, Integer> weakMap = new WeakHashMap<>();

        String key1 = new String("A");
        Integer value1 = 1;
        weakMap.put(key1, value1);

        String key2 = new String("B");
        Integer value2 = 2;
        weakMap.put(key2, value2);

        System.out.println("A-nın dəyəri: " + weakMap.get("A"));
        System.out.println("B-nin dəyəri: " + weakMap.get("B"));

        key1 = null;

        System.gc();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("A-nın dəyəri (GC-dən sonra): " + weakMap.get("A"));
        System.out.println("B-nin dəyəri: " + weakMap.get("B"));
    }
}
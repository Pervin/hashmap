import java.util.TreeMap;

public class TreeMapExample {
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();

        treeMap.put(3, "Üç");
        treeMap.put(1, "Bir");
        treeMap.put(2, "İki");

        for (Integer key : treeMap.keySet()) {
            System.out.println(key + " => " + treeMap.get(key));
        }
    }
}
